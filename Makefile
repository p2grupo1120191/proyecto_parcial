
bin/prueba: obj/prueba.o obj/Ejemplo.o obj/slaballoc.o obj/Casa.o obj/Avion.o obj/Planeta.o
	gcc -g obj/prueba.o obj/Ejemplo.o obj/slaballoc.o obj/Casa.o obj/Avion.o obj/Planeta.o -o bin/prueba		#agregue los archivos .o que necesite

obj/prueba.o: src/prueba.c
	gcc -g -Wall -c -Iinclude/ src/prueba.c -o obj/prueba.o

obj/Ejemplo.o: src/Ejemplo.c
	gcc -g -Wall -c -Iinclude/ src/Ejemplo.c -o obj/Ejemplo.o

obj/slaballoc.o: src/slaballoc.c
	gcc -g -Wall -c -Iinclude/ src/slaballoc.c -o obj/slaballoc.o

obj/Casa.o: src/Casa.c
	gcc -g -Wall -c -Iinclude/ src/Casa.c -o obj/Casa.o

obj/Avion.o: src/Avion.c
	gcc -g -Wall -c -Iinclude/ src/Avion.c -o obj/Avion.o

obj/Planeta.o: src/Planeta.c
	gcc -g -Wall -c -Iinclude/ src/Planeta.c -o obj/Planeta.o

#agregue las reglas que necesite


.PHONY: clean
clean:
	rm bin/* obj/*.o
