#include <unistd.h>
#include <stdlib.h>
#include "../include/objetos.h"


void crear_Avion(void *ref, size_t tamano){
	Avion *buf = (Avion *)ref;
	buf->pasajeros=0;
	buf->nombre_piloto = NULL;
}
//Destructor
void destruir_Avion(void *ref, size_t tamano){
	Avion *buf = (Avion *)ref;
	buf->pasajeros=0;
	buf->nombre_piloto=NULL;
}
